    <!-- Login Modal -->
    <!-- Modal -->
    <div class="modal fade" id="loginModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content login-modal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><i class="fa fa-sign-in"></i>LOGIN</h4>
                </div>
                <form action="login/process" method="post">
                    <div class="modal-body">
                        <div>
                            <label><i class="fa fa-user"></i>NIS</label>
                            <input class="form-control" name="nis" type="text" required placeholder="Username/Email">
                        </div>
                        <div>
                            <label><i class="fa fa-key"></i>PASSWORD</label>
                            <input class="form-control" name="password" type="password" required placeholder="Password">
                        </div>
                        <br>
                        <div class="text-center">
                        <input type="submit" class="btn btn-success btn-block" value="Login" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>