<div class="row nav-row trans-menu">
        <div class="container nav-container">
            <div class="top-navbar">
                <div class="pull-right">
                    <div class="top-nav-social pull-left">

                    </div>
                    <div class="top-nav-login-btn pull-right">
                        <?php                
                            if($this->session->userdata('siswa_id')){
                                $tahun_ajar = $this->db->get_where('tahun_ajar',array('status_id'=>13))->result_array();
                                $q = "
                                    select km.kelas_map_id, k.nama as kelas from kelas_map_siswa kms
                                    left join kelas_map km on (kms.kelas_map_id=km.kelas_map_id)
                                    left join kelas k on (k.kelas_id=km.kelas_id)
                                    where siswa_id = ".$this->session->userdata('siswa_id')."
                                    and tahun_ajar_id = ".$tahun_ajar[0]['tahun_ajar_id'];      
                                $kelas = $this->db->query($q)->result_array();                                 
                                echo '<a href="beranda" ><i class="fa fa-user"></i> '.$this->session->userdata('siswa_nama').' ('.$kelas[0]['kelas'].')</a> ';
                                echo '<a href="login/logout" ><i class="fa fa-sign-out"></i>LOGOUT</a>';
                            }
                            else{
                                echo '<a href="#" data-toggle="modal" data-target="#loginModal"><i class="fa fa-sign-in"></i>LOGIN</a>';
                            }
                        ?>
                        
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
            <nav id="pathshalaNavbar" class="navbar navbar-default" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#pathshalaNavbarCollapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html">SD Kristen<br>Sokaraja</a>
                </div>
                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="pathshalaNavbarCollapse">
                    <ul class="nav navbar-nav">
                        <li><a href="welcome"><i class="fa fa-home"></i>Beranda</a></li>
                        <li><a href="profil_sekolah"><i class="fa fa-building-o"></i>Profil Sekolah</a></li>
                        <li><a href="ekstra"><i class="fa fa-soccer-ball-o"></i>Ekstrakurikuler</a></li>
                        <li><a href="prestasi"><i class="fa fa-trophy"></i>Prestasi</a></li>
                        <li><a href="guru"><i class="fa fa-users"></i>Guru</a></li>
                        <li><a href="galeri"><i class="fa fa-file-image-o"></i>Galeri</a></li>
                        <li><a href="hubungi"><i class="fa fa-home"></i>Hubungi Kami</a></li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </nav>
        </div>
    </div>