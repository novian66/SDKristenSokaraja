<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profil extends MX_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->c_auth->checkLogin();
		$this->load->model('m_global');
		$this->load->model('m_siswa');
		$this->siswa_id = $this->session->userdata('siswa_id');
	}	
	public function index()
	{
		$data['list_is_active'] = $this->m_siswa->get_list_is_active();
		$data['list_agama'] = $this->m_siswa->get_list_agama();
		$data['list_jenis_kelamin'] = $this->m_siswa->get_list_jenis_kelamin();
		$data['siswa'] = $this->db->get_where('siswa', array('siswa_id' => $this->siswa_id))->result_array();
		$this->load->view('profil',$data);
	}

	public function do_update() {
		$post = $this->input->post();
		$id = $post['siswa_id'];
		$data_update = array(
		'nis' 				=> $post['nis'],
		'nama'         		=> $post['nama'],
		'tanggal_lahir' 	=> $post['tanggal_lahir'],
		'nama_ortu' 		=> $post['nama_ortu'],
		'email_ortu' 		=> $post['email_ortu'],
		'no_telp' 			=> $post['no_telp'],
		'telegram' 			=> $post['telegram'],
		'tanggal_masuk' 	=> $post['tanggal_masuk'],
		'alamat' 			=> $post['alamat']
		);

		if (isset($post['password'])) {
			$data_update['password'] = $this->encryption->encrypt($post['password']);
		}
		$result_update = $this->m_siswa->update($id, $data_update);
		if ($result_update) {
				$this->session->set_flashdata('success', 'Data success updated!');
		} else {
				$this->session->set_flashdata('error', 'Data cannot updated!');
		}
		redirect(base_url().'profil');
	}
}
