<!DOCTYPE html>
<html>

<head>

  <?php include "template/head.php" ?>

</head>

<body>
    <?php include "template/topbar.php" ?>
		<!-- Page Title Section -->
		<div class="row page-title page-title-about">
			<div class="container">
				<h2></i>PROFIL SISWA</h2>
			</div>
		</div>





	<!-- Events Section -->
		<!-- Events Section -->
		<div class="row section-row evets-row">
			<div class="container">
				<div class="col-md-8 left-event-items">
                <?php
                                    $error = $this->session->flashdata('error');
                                    if ($error) {
                                        ?>
                                        <div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                            <?php echo $error; ?> <a href="#" class="alert-link">Error!</a>.
                                        </div> 
                                    <?php } ?>
                                    <?php
                                    $success = $this->session->flashdata('success');
                                    if ($success) {
                                        ?>
                                        <div class="alert alert-success alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                                            <?php echo $success; ?> <a href="#" class="alert-link">Success!</a>.
                                        </div> 
                                    <?php } ?> 
                    <form action="profil/do_update" method="post">
                        <div class="col-md-6">
                            <div class="col-sm-12">
                                <label class="col-form-label col-2">NIS</label>
                                <input type="hidden" class="form-control col-9" name="siswa_id" value="<?php echo $siswa[0]['siswa_id'] ?>" required="">
                                <input type="number" class="form-control col-9" name="nis" value="<?php echo $siswa[0]['nis'] ?>" readonly required="">
                            </div>
                            <div class="col-sm-12">
                                <label class="col-form-label col-2">Nama</label>
                                <input type="text" class="form-control col-9" name="nama" value="<?php echo $siswa[0]['nama'] ?>" readonly required="">
                            </div>
                            <div class="col-sm-12">
                                <label class="col-form-label col-2">Tanggal Lahir</label>
                                <input type="date" class="form-control col-9" value="<?php echo $siswa[0]['tanggal_lahir'] ?>" readonly name="tanggal_lahir" required="">
                            </div>
                            <div class="col-sm-12">
                                <label class="col-form-label col-2">Jenis Kelamin</label>
                                <select class="form-control col-9" disabled name="jenis_kelamin_id" required="">
                                <?php foreach ($list_jenis_kelamin as $value): ?>
                                    <option value="<?php echo $value['data_combo_id'] ?>"><?php echo $value['nama'] ?></option>
                                <?php endforeach ?>
                                </select>
                            </div>
                            <div class="col-sm-12">
                                <label class="col-form-label col-2">Nama Ortu</label>
                                <input type="text" class="form-control col-9" readonly value="<?php echo $siswa[0]['nama_ortu'] ?>" name="nama_ortu" required="">
                            </div>
                            <div class="col-sm-12">
                                <label class="col-form-label col-2">Agama</label>
                                <select class="form-control col-9" name="agama_id" disabled required="">
                                <?php foreach ($list_agama as $value): ?>
                                    <option value="<?php echo $value['data_combo_id'] ?>"><?php echo $value['nama'] ?></option>
                                <?php endforeach ?>
                                </select>
                            </div>
                            <div class="col-sm-12">
                                <label class="col-form-label col-2">Ubah Password</label>
                                <input type="password" class="form-control col-9" value="" name="nama_ortu" placeholder="Kosongkan Jika Tidak Ingin Ganti Password" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="col-sm-12">
                                <label class="col-form-label col-2">Email Ortu</label>
                                <input type="email" class="form-control col-9" value="<?php echo $siswa[0]['email_ortu'] ?>" name="email_ortu" required="">
                            </div>
                            <div class="col-sm-12">
                                <label class="col-form-label col-2">Telp</label>
                                <input type="text" class="form-control col-9" readonly name="no_telp" value="<?php echo $siswa[0]['no_telp'] ?>" required="">
                            </div>
                            <div class="col-sm-12">
                                <label class="col-form-label col-2">Telegram</label>
                                <input type="text" class="form-control col-9" name="telegram" value="<?php echo $siswa[0]['telegram'] ?>" required="">
                            </div>
                            <div class="col-sm-12">
                                <label class="col-form-label col-2">Alamat</label>
                                <input type="text" class="form-control col-9" readonly name="alamat" value="<?php echo $siswa[0]['alamat'] ?>" required="">
                            </div>
                            <div class="col-sm-12">
                                <label class="col-form-label col-2">Tanggal Masuk</label>
                                <input type="date" class="form-control col-9" name="tanggal_masuk" readonly value="<?php echo $siswa[0]['tanggal_masuk'] ?>" required="">
                            </div>
                            <div class="col-sm-12">
                                <label class="col-form-label col-2">Status Aktif</label>
                                <select class="form-control col-9" name="status_id" disabled required="">
                                <?php foreach ($list_is_active as $value): ?>
                                    <option value="<?php echo $value['data_combo_id'] ?>"><?php echo $value['nama'] ?></option>
                                <?php endforeach ?>
                                </select>
                            </div>
                        </div>
                        <div style="margin:10px" class="col-md-12 text-center">
                            <input type="submit" class="btn btn-primary" value="Simpan">
                        </div>
                    </form>
				</div>                
				<div class="col-md-4 right-event-items">
					<div class="event-item categories-list">
						<div class="sidebar-box">
							<h5><i class="fa fa-list"></i>CATEGORIES</h5>
							<div class="inner-content-box">
								<ul class="list-group">
									<li class="list-group-item"><a href="beranda"><i class="fa fa-comment"></i>INFORMASI</a></li>
									<li class="list-group-item"><a href="profil"><i class="fa fa-user"></i>PROFIL</a></li>
									<li class="list-group-item"><a href="jadwal"><i class="fa fa-calendar"></i>JADWAL</a></li>
									<li class="list-group-item"><a href="nilai"><i class="fa fa-book"></i>NILAI</a></li>								
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>



    <?php include "template/footer.php" ?>
    
    <?php include "template/loginModal.php" ?>


    <?php include "template/scripts.php" ?>

    <script>
        $(document).ready(function(){
            $("select[name='jenis_kelamin_id']").val(<?php echo $siswa[0]['jenis_kelamin_id'] ?>).attr('selected', 'selected');
            $("select[name='agama_id']").val(<?php echo $siswa[0]['agama_id'] ?>).attr('selected', 'selected');
            $("select[name='status_id']").val(<?php echo $siswa[0]['status_id'] ?>).attr('selected', 'selected');
        });
    </script>
</body>

</html>