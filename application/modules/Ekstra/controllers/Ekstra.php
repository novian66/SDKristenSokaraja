<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ekstra extends MX_Controller {
	public function index()
	{
		$data['page'] = $this->db->get_where('profil', array('profil_id' => '3'))->result_array();
		$this->load->view('ekstra',$data);
	}
}
