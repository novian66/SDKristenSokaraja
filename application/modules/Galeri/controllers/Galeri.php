<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galeri extends MX_Controller {
	public function index()
	{
		$data['galeri'] = $this->db->get_where('galeri', array())->result_array();
		$this->load->view('galeri',$data);
	}
}
