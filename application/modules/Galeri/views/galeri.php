<!DOCTYPE html>
<html>

<head>

  <?php include "template/head.php" ?>

</head>

<body>
    <?php include "template/topbar.php" ?>
		<!-- Page Title Section -->
		<div class="row page-title page-title-about">
			<div class="container">
				<h2></i>GALERI</h2>
			</div>
		</div>




    <!-- Principal Intro Section -->
    <div class="row principal-intro-row">
        <div class="container">
		<!-- START: GALLERY -->
		<div class="row gallery-row">
			<div class="container clear-padding">
				<div class="image-set">
					<?php foreach($galeri as $val) { ?>
					<div class="col-md-4 col-sm-4">
						<div class="image-wrapper">
							<img src="../admin.sdkristensokaraja/assets/galeri/<?php echo $val['gambar'] ?>" alt="<?php echo $val['nama'] ?>">
							<div class="img-caption">
								<div class="link">
									<a target="blank" title="<?php echo $val['nama'] ?>" href="../admin.sdkristensokaraja/assets/galeri/<?php echo $val['gambar'] ?>">
										<i class="fa fa-plus"></i>
									</a>
								</div>
							</div>
						</div>
					</div>				
					<?php } ?>
				
				</div>
			</div>
		</div>
		<!-- END: GALLERY -->
			

        </div>
    </div>


    <?php include "template/footer.php" ?>
    
    <?php include "template/loginModal.php" ?>


    <?php include "template/scripts.php" ?>


</body>

</html>