<!DOCTYPE html>
<html>

<head>

  <?php include "template/head.php" ?>

</head>

<body>
    <?php include "template/topbar.php" ?>
		<!-- Page Title Section -->
		<div class="row page-title page-title-about">
			<div class="container">
				<h2></i>NILAI</h2>
			</div>
		</div>





	<!-- Events Section -->
		<!-- Events Section -->
		<div class="row section-row evets-row">
			<div class="container">
				<div class="col-md-8 left-event-items">
                    <?php
                    $error = $this->session->flashdata('error');
                    if ($error) {
                        ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                            <?php echo $error; ?> <a href="#" class="alert-link">Error!</a>.
                        </div> 
                    <?php } ?>
                    <?php
                    $success = $this->session->flashdata('success');
                    if ($success) {
                        ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                            <?php echo $success; ?> <a href="#" class="alert-link">Success!</a>.
                        </div> 
                    <?php } ?> 
                    <h3>Semester 1</h3>
                    <table class="table table-responsive table-hover">
                        <thead>
                            <th>No</th>
                            <th>Pelajaran</th>
                            <th>Nilai</th>
                        </thead>
                        <?php 
                        $no = 1;
                        foreach($nilai1 as $val){ ?>
                        <tr>
                            <td><?php echo $no ?></td>
                            <td><?php echo $val['pelajaran'] ?></td>                
                            <td><?php echo $val['nilai'] ?></td>              
                        </tr>
                        <?php $no++; } ?>
                    </table>     
                    <br>
                    <h3>Semester 2</h3>
                    <table class="table table-responsive table-hover">
                        <thead>
                            <th>No</th>
                            <th>Pelajaran</th>
                            <th>Nilai</th>
                        </thead>
                        <?php 
                        $no = 1;
                        foreach($nilai2 as $val){ ?>
                        <tr>
                            <td><?php echo $no ?></td>
                            <td><?php echo $val['pelajaran'] ?></td>                
                            <td><?php echo $val['nilai'] ?></td>                
                        </tr>
                        <?php $no++; } ?>
                    </table>  
				</div>                
				<div class="col-md-4 right-event-items">
					<div class="event-item categories-list">
						<div class="sidebar-box">
							<h5><i class="fa fa-list"></i>CATEGORIES</h5>
							<div class="inner-content-box">
								<ul class="list-group">
									<li class="list-group-item"><a href="beranda"><i class="fa fa-comment"></i>INFORMASI</a></li>
									<li class="list-group-item"><a href="profil"><i class="fa fa-user"></i>PROFIL</a></li>
									<li class="list-group-item"><a href="jadwal"><i class="fa fa-calendar"></i>JADWAL</a></li>
									<li class="list-group-item"><a href="nilai"><i class="fa fa-book"></i>NILAI</a></li>								
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>



    <?php include "template/footer.php" ?>
    
    <?php include "template/loginModal.php" ?>


    <?php include "template/scripts.php" ?>

    <script>
        $(document).ready(function(){
            $("select[name='jenis_kelamin_id']").val(<?php echo $siswa[0]['jenis_kelamin_id'] ?>).attr('selected', 'selected');
            $("select[name='agama_id']").val(<?php echo $siswa[0]['agama_id'] ?>).attr('selected', 'selected');
            $("select[name='status_id']").val(<?php echo $siswa[0]['status_id'] ?>).attr('selected', 'selected');
        });
    </script>
</body>

</html>