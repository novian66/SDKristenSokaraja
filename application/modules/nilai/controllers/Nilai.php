<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nilai extends MX_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->c_auth->checkLogin();
		$this->load->model('m_global');
	}	
	public function index()
	{
		$data['nilai1'] = $this->m_global->get_nilai(9);
		$data['nilai2'] = $this->m_global->get_nilai(10);
		$this->load->view('nilai',$data);
	}

}
