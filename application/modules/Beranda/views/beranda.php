<!DOCTYPE html>
<html>

<head>

  <?php include "template/head.php" ?>

</head>

<body>
    <?php include "template/topbar.php" ?>
		<!-- Page Title Section -->
		<div class="row page-title page-title-about">
			<div class="container">
				<h2></i>INFORMASI</h2>
			</div>
		</div>




	<!-- Events Section -->
		<!-- Events Section -->
		<div class="row section-row evets-row">
			<div class="container">
				<div class="col-md-8 left-event-items">
					<?php foreach($informasi as $val){ ?>
						<div class="event-item">
							<div class="col-sm-12 event-single-wrapper">
							<h3><?php echo $val['judul'] ?></h3>
							<h5>
								<span><i class="fa fa-calendar"></i><?php echo date('d-M-Y',strtotime($val['tanggal'])) ?></span>
								<span><i class="fa fa-info"></i><?php echo $val['nama_kelas'] ?></span>
							</h5>							
							<?php echo $val['isi'] ?>
							</div>
							<div class="clearfix"></div>
						</div>
					<?php } ?>

				</div>
				<div class="col-md-4 right-event-items">
					<div class="event-item categories-list">
						<div class="sidebar-box">
							<h5><i class="fa fa-list"></i>CATEGORIES</h5>
							<div class="inner-content-box">
								<ul class="list-group">
									<li class="list-group-item"><a href="beranda"><i class="fa fa-comment"></i>INFORMASI</a></li>
									<li class="list-group-item"><a href="profil"><i class="fa fa-user"></i>PROFIL</a></li>
									<li class="list-group-item"><a href="jadwal"><i class="fa fa-calendar"></i>JADWAL</a></li>
									<li class="list-group-item"><a href="nilai"><i class="fa fa-book"></i>NILAI</a></li>								
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>




    <?php include "template/footer.php" ?>
    
    <?php include "template/loginModal.php" ?>


    <?php include "template/scripts.php" ?>


</body>

</html>