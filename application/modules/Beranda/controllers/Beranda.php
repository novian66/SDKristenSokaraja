<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends MX_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->c_auth->checkLogin();
		$this->load->model('m_global');
		$this->siswa_id = $this->session->userdata('siswa_id');
	}
	public function index()
	{
		$tahun_ajar = $this->db->get_where('tahun_ajar',array('status_id'=>13))->result_array();
		$data['informasi'] = $this->m_global->getInformasi($tahun_ajar[0]['tahun_ajar_id']);
		$this->load->view('beranda',$data);
	}
}
