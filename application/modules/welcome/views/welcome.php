<!DOCTYPE html>
<html>

<head>

  <?php include "template/head.php" ?>

</head>

<body>
    <?php include "template/topbar.php" ?>
    <?php include "template/slider.php" ?>



    <!-- Principal Intro Section -->
    <div class="row principal-intro-row">
        <div class="container">
            <div class="col-sm-12 principal-intro">
                <?php echo $page[0]['isi'] ?>
            </div>
        </div>
    </div>


    <?php include "template/footer.php" ?>
    
    <?php include "template/loginModal.php" ?>


    <?php include "template/scripts.php" ?>


</body>

</html>