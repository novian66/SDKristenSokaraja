<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MX_Controller {
	public function index()
	{
		$data['page'] = $this->db->get_where('profil', array('profil_id' => '1'))->result_array();
		$this->load->view('welcome',$data);
	}
}
