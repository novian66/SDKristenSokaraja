<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal extends MX_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->c_auth->checkLogin();
		$this->load->model('m_global');
	}	
	public function index()
	{
		$data['jadwal'] = $this->m_global->get_jadwal();
		$this->load->view('jadwal',$data);
	}

}
