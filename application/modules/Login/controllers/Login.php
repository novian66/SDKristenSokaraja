<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	public function __construct()
  {
		parent::__construct();		
		$this->siswa_id = $this->session->userdata('siswa_id');
  } 
	public function index()
	{
		$this->load->view('login');
	}

	public function process(){          
		$siswa = $this->db->get_where('siswa', array('nis' => $_POST['nis'], 'status_id' => 13))->result_array();  
		if(empty($siswa)){
				$this->session->set_flashdata('message', array('condition'=>'warning','text'=>'Login gagal1.'));
				$this->session->set_flashdata('content', $_POST);
				header("location: ".base_url()."login");	
				die();
		}
		else{        
			$password = $this->encryption->decrypt($siswa[0]['password']);
			if($password==$_POST['password']){
				$this->session->set_userdata('siswa_id',$siswa[0]['siswa_id']);
				$this->session->set_userdata('siswa_nama',$siswa[0]['nama']);
				header("location: ".base_url()."beranda");	
				die();
			}
      $this->session->set_flashdata('message', array('condition'=>'warning','text'=>'Login gagal2.'));
			$this->session->set_flashdata('content', $_POST);
			header("location: ".base_url()."login");
			die();
		}
	
	}

	public function logout(){
		session_destroy();
		header("location: ".base_url()."welcome");
	}	
}
