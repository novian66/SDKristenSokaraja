
<!DOCTYPE html>
<html lang="en">
<head>
	<title>Admin Kluban</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="assets/images/favicon.png"/>
	
<!--===============================================================================================-->
<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/login/util.css">
	<link rel="stylesheet" type="text/css" href="assets/login/main.css">
<!--===============================================================================================-->
</head>
<body>

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form" method="post" action="login/process">
					<div class="text-center ">
						<img width="100%" src="assets/images/logo.png">
					</div>
					<br>
                    <?php
                    $message = $this->session->flashdata('message');
                    $content = $this->session->flashdata('content');
                    if ($message) {
                        ?>
                        <br>
                        <p class=""><?php echo $message['text'] ?></p>
                    <?php } ?> 
					<br>
					<div class="wrap-input100 validate-input">
						<input class="input100" type="text" name="nis" required id="username">
						<span class="focus-input100" data-placeholder="Username"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">

						<input class="input100" type="password" name="password" required id="password">
						<span class="focus-input100" data-placeholder="Password"></span>
					</div>

					<div class="text-center ">
							<button class="btn btn-success">Log In</button>
					</div>

				</form>
			</div>
		</div>
	</div>


	<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
<script type="text/javascript" src="assets/js/jQuery_v3_2_0.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="assets/login/main.js"></script>
</body>
</html>
