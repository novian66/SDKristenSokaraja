<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prestasi extends MX_Controller {
	public function index()
	{
		$data['prestasi_list'] = $this->db->get_where('prestasi', array('status_id' => '13' ))->result_array();
		$this->load->view('prestasi',$data);
	}
}
