<!DOCTYPE html>
<html>

<head>

  <?php include "template/head.php" ?>

</head>

<body>
    <?php include "template/topbar.php" ?>
		<!-- Page Title Section -->
		<div class="row page-title page-title-about">
			<div class="container">
				<h2></i>PRESTASI</h2>
			</div>
		</div>




    <!-- Principal Intro Section -->
    <div class="row principal-intro-row">
        <div class="container">
            <div class="col-sm-12 principal-intro">
                <div class="about-row">

                    <?php foreach($prestasi_list as $val){ ?>

                    <div class="col-sm-6 col-md-4">
                        <div class="we-offer-item">                            
                            <h5><?php echo $val['nama'] ?></h5>
                            <p>
                                <?php echo $val['deskripsi'] ?>
                            </p>
                        </div>
                    </div>

                    <?php } ?>

                </div>

            </div>
        </div>
    </div>


    <?php include "template/footer.php" ?>
    
    <?php include "template/loginModal.php" ?>


    <?php include "template/scripts.php" ?>


</body>

</html>