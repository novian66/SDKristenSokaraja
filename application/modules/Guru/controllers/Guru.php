<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guru extends MX_Controller {
	public function index()
	{
		$data['guru_list'] = $this->db->get_where('karyawan', array('status_id' => '13','jabatan_id in (5,6)' => null ))->result_array();
		$this->load->view('guru',$data);
	}
}
