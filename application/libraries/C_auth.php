<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_auth {
    public function __construct()
    {
        $this->CI =& get_instance();
    }    
    function checkLogin(){
        if($this->CI->session->userdata('siswa_id')==null){
            header("location: ".base_url()."login");
            die();
        }
    }

    function getAdmin(){
        if($this->CI->session->userdata('karyawan_id')!=null){
            $q="
            select *,  
            (select nama from tahun_ajar where tahun_ajar_id = ".$this->CI->session->userdata('tahun_ajar_id')." ) as tahun_ajar 
            from karyawan where karyawan_id = ".$this->CI->session->userdata('karyawan_id')."
            limit 1        
            ";
            $result = $this->CI->db->query($q)->result_array();
            return $result[0];
        }
    }

}

?>