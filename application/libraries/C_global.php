<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_global {
    public function __construct()
    {
        $this->CI =& get_instance();
    }    

    function data_list(){
        $q="
        select km.*, kelas.nama as kelas from kelas_map km
        left join kelas kelas on (kelas.kelas_id=km.kelas_id)
        where tahun_ajar_id = ".$this->CI->session->userdata('tahun_ajar_id')."
        order by kelas.nama
        ";
        $result['kelas'] = $this->CI->db->query($q)->result_array();
        return $result;
    }

    function kelas_pelajaran(){
        $q="
        SELECT
            a.*,
            b.`nama` AS `pelajaran_name`,
            d.`nama` AS `kelas_name`
        FROM kelas_map_pelajaran a
        LEFT JOIN pelajaran b ON b.`pelajaran_id` = a.`pelajaran_id`
        LEFT JOIN kelas_map c ON c.`kelas_map_id` = a.`kelas_map_id`
        LEFT JOIN kelas d ON d.`kelas_id` = c.`kelas_id`
        WHERE c.`tahun_ajar_id` = '".$this->CI->session->userdata('tahun_ajar_id')."'
        AND a.`karyawan_id` = '".$this->CI->session->userdata('karyawan_id')."'
        ";
        $result['kelas_pelajaran'] = $this->CI->db->query($q)->result_array();
        return $result;
    }

    public function list_semester() {
        $q="
        SELECT *
        FROM data_combo
        WHERE group_list = 'semester'
        ";
        $result['semester'] = $this->CI->db->query($q)->result_array();
        return $result;  
    }
}

?>