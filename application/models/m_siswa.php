<?php
class m_siswa extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }       

    public function data_siswa() {
        $q="
        SELECT 
            a.*,
            b.`nama` AS `name_is_active`,
            c.`nama` AS `agama_name`,
            e.`nama` AS `jenis_kelamin_name`
        FROM siswa a
        LEFT JOIN data_combo b ON b.`data_combo_id` = a.`status_id`
        LEFT JOIN data_combo c ON c.`data_combo_id` = a.`agama_id`
        LEFT JOIN data_combo e ON e.`data_combo_id` = a.`jenis_kelamin_id`
        ";
        return $this->db->query($q)->result_array();	 
    }     

    public function get_siswa_by_id($id){
        $q="
        SELECT * FROM siswa WHERE siswa_id = $id
        ";
        return $this->db->query($q)->result_array();	 
    }

    public function get_list_is_active() {
        $q="
        SELECT *
        FROM data_combo
        WHERE data_combo_id = 12 OR data_combo_id = 13
        ";
        return $this->db->query($q)->result_array();     
    }

    public function get_list_agama() {
        $q="
        SELECT *
        FROM data_combo
        WHERE group_list = 'agama'
        ";
        return $this->db->query($q)->result_array();     
    }

    public function get_list_jenis_kelamin() {
        $q="
        SELECT *
        FROM data_combo
        WHERE group_list = 'jk'
        ";
        return $this->db->query($q)->result_array();     
    }

    public function insert($insert) {
        $this->db->insert('siswa', $insert);
        return true;
    }

    public function update($id, $data_update) {
        $this->db->where('siswa_id', $id);
        $this->db->update('siswa', $data_update);
        return true;
    }

    public function delete($id) {
        $this->db->delete('siswa', array('siswa_id' => $id));
        return true;
    }
}