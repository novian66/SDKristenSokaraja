<?php
class m_global extends CI_Model {

    public function __construct()
    {
        parent::__construct();
        $this->siswa_id = $this->session->userdata('siswa_id');
        $tahun_ajar = $this->db->get_where('tahun_ajar',array('status_id'=>13))->result_array();
        $q = "
            select km.kelas_map_id, k.nama as kelas from kelas_map_siswa kms
            left join kelas_map km on (kms.kelas_map_id=km.kelas_map_id)
            left join kelas k on (k.kelas_id=km.kelas_id)
            where siswa_id = ".$this->session->userdata('siswa_id')."
            and tahun_ajar_id = ".$tahun_ajar[0]['tahun_ajar_id'];      
        $kelas = $this->db->query($q)->result_array();    
        $this->kelas_map_id = $kelas[0]['kelas_map_id']; 
        $this->tahun_ajar_id = $tahun_ajar[0]['tahun_ajar_id']; 
        // Your own constructor code
    }       

    public function getInformasi($tahun_ajar_id) {
        $q="
        select
        judul,
        tanggal,
        isi,
        coalesce(concat('Kelas ',k.nama),'Umum') as nama_kelas
        from informasi inf
        left join kelas_map km on (km.kelas_map_id=inf.kelas_map_id)
        left join kelas k on (km.kelas_id=k.kelas_id)
        where km.kelas_map_id = (
                select km.kelas_map_id from kelas_map_siswa kms
                left join kelas_map km on (kms.kelas_map_id=km.kelas_map_id)
                where siswa_id = $this->siswa_id
                and tahun_ajar_id = $tahun_ajar_id
                limit 1
            )
        or tahun_ajar_id is null
        order by inf.tanggal desc
        ";
        return $this->db->query($q)->result_array();	 
    }     

    public function get_jadwal(){
        $q="
        select 
        dc.nama as hari,
        jp.jam, 
        p.nama as pelajaran, 
        k.nama as guru
        from jadwal_pelajaran jp
        left join kelas_map_pelajaran kmp on (kmp.kelas_map_pelajaran_id=jp.kelas_map_pelajaran_id)
        left join pelajaran p on (p.pelajaran_id=kmp.pelajaran_id)
        left join data_combo dc on (dc.data_combo_id=jp.hari_id)
        left join kelas_map km on (km.kelas_map_id=kmp.kelas_map_id)
        left join karyawan k on (k.karyawan_id=kmp.karyawan_id)
        where kmp.kelas_map_id = $this->kelas_map_id
        order by jp.hari_id, jp.jam
        ";
        return $this->db->query($q)->result_array();	 
    }

    public function get_nilai($semester){
        $q="
        select 
        jp.kelas_map_pelajaran_id,
        dc.nama as hari,
        jp.jam, 
        p.nama as pelajaran, 
        k.nama as guru,
        coalesce(nilai.nilai,'Belum di Nilai') as nilai
        from jadwal_pelajaran jp
        left join kelas_map_pelajaran kmp on (kmp.kelas_map_pelajaran_id=jp.kelas_map_pelajaran_id)
        left join pelajaran p on (p.pelajaran_id=kmp.pelajaran_id)
        left join data_combo dc on (dc.data_combo_id=jp.hari_id)
        left join kelas_map km on (km.kelas_map_id=kmp.kelas_map_id)
        left join karyawan k on (k.karyawan_id=kmp.karyawan_id)
        left join (
                            SELECT
                            n.kelas_map_pelajaran_id,
                            ta.nama,
                            dc.nama as semester,
                            p.nama as pelajaran,
                            nilai
                            FROM `nilai` n
                            left join kelas_map_pelajaran kmp on (kmp.kelas_map_pelajaran_id=n.kelas_map_pelajaran_id)
                            left join kelas_map km on (km.kelas_map_id=kmp.kelas_map_id)
                            left join pelajaran p on (p.pelajaran_id=kmp.pelajaran_id)
                            left join data_combo dc on (dc.data_combo_id=n.semester_id)
                            left join tahun_ajar ta on (ta.tahun_ajar_id=km.tahun_ajar_id)
                            where ta.tahun_ajar_id = $this->tahun_ajar_id
                            and n.siswa_id = $this->siswa_id
                            and n.semester_id = $semester
                            ) nilai on (nilai.kelas_map_pelajaran_id=jp.kelas_map_pelajaran_id)
        where kmp.kelas_map_id = $this->kelas_map_id
        order by jp.hari_id, jp.jam
        ";
        return $this->db->query($q)->result_array();	 
    }    
}